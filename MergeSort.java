/*
    AUTHOR: Suraj Naranatt; DATE: 01SEP2020
    Implementation of recursive merge sort algorithm.
*/

class MergeSort{
    static void sort(int[] array, int start, int end){
        int size = end-start+1;         //O(1)
        if(size <= 1)                   //O(1)
            return;
        
        int mid = start + size/2 - 1;   //O(1)

        /*
            Becase each time, the size of the array is halved, the number of times the above comparisons
            are executed depends on the size of the array.
        */
        
        //Multiplying by n because each time a merge happens.
        sort(array,start,mid);          //O(log-base2(n))
        sort(array,mid+1,end);          //O(log-base2(n))
        merge(array,start, mid+1,end);  //O(n)
    }
    static void merge(int[] array, int start, int mid, int end){
        int[] temp = new int[end-start+1];      //O(1)
        int i = start, j = mid, k = 0;          //O(1)
        while(i < mid && j <= end){             //O(n)
            if(array[i] <= array[j])            //O(1)
                temp[k++] = array[i++];         //O(1)
            else
                temp[k++] = array[j++];         //O(1)
        }
        if(i >= mid)                            //O(1)
            while(j <= end)                     //O(n)
                temp[k++] = array[j++];         //O(1)
        else
            while(i < mid)                      //O(n)
                temp[k++] = array[i++];         //O(1)

        for(int l = 0; l < k; l++)              //O(n)
            array[start+l] = temp[l];           //O(1)
    }
    public static void main(String[] args){
        int[] arr = {6,5,4,3,2,1};
        
        //Printing the original array.
        System.out.print("Unsorted Array: ");
        for(short i = 0; i < arr.length; i++)
            System.out.print(""+arr[i]+"|");
        System.out.println("");
        sort(arr,0,5);
        
        //Printing the sorted array.
        System.out.println("++++++++++++++");
        System.out.print("Sorted Array: ");
        for(int i = 0; i < 6; i++)
            System.out.print(""+arr[i]+"|");
    }
}