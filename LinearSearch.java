/*
    AUTHOR: Suraj Naranatt; DATE: 2020-07-29
    Program implementing linear search algorithm.
*/

import java.util.Scanner;

class LinearSearch{
    public static void main(String[] args){
        int searchElement, elementCount;        //O(1)
        Scanner input = new Scanner(System.in); //O(1)
        
        System.out.print("№ of Elements: ");    //O(1)
        elementCount = input.nextInt();         //O(1)
		int elements[] = new int[elementCount]; //O(1)

        for(int i = 0; i < elementCount; i++){  //O(n) ∵ elementCount = n = input size
            System.out.print("Element "+i+" : ");   //O(1)
            elements[i] = input.nextInt();      //O(1)
        }
        System.out.print("Search for:: ");      //O(1)
        searchElement = input.nextInt();        //O(1)
        input.close();  //O(1)

        int count;  //O(1)
        for(count = 0; count < elementCount; count++){  //O(n) ∵ elementCount = n = input size
            if(elements[count] == searchElement){   //O(1)
                System.out.println("Found.\nIterations = "+count);  //O(1)
                break;  //O(1)
            }
        }
        if(count == elements.length)   //O(1)
            System.out.println("Not found.");   //O(1)

    }
}
