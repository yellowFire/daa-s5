/*
	AUTHOR: Suraj Naranatt; DATE: 25AUG2020
	Program implementing recursive binary search algorithm.
 */

 import java.util.Scanner;
 
 class BinSearchRecursive{
	 static int BinSearch(int[] array, int first, int last, int element){
		if(first > last)	//O(1)
			 return -1;
		else{
			int mid = (first+last)/2;	//O(1)
			if(array[mid] == element)	//O(1)
				return mid;
			else if(array[mid]>element)
				return BinSearch(array, first, mid-1, element);
			else
				return BinSearch(array, mid+1, last, element);
		}
	 }

	 static void printArray(int[] array){
		 System.out.print("Array: ");			//O(1)
		 for(int i = 0; i < array.length; i++) 		//O(n)
			System.out.print(""+array[i]+"|");	//O(1)
		 System.out.println("");			//O(1)
	 }

	 public static void main(String[] args){
		 /*
		 'array' is the array containing a set of elements.
		 'element' refers to the element being searched for.
		 'foundIndex' is the index at which the element is found.
		 */
		int[] array = {0,1,2,3,5,6,9};	//O(1)
		int element, foundIndex;	//O(1)
		Scanner input = new Scanner(System.in);		//O(1)

		printArray(array);

		System.out.print("Enter the element to search: ");	//O(1)
		element = input.nextInt();	//O(1)
		foundIndex = BinSearch(array, 0, array.length-1, element);	//O(logn)

		// If 'foundIndex' is equal to -1, then the element does not exist in the array.
		if(foundIndex == -1)
			System.out.println("Not Found.");	//O(1)
		else
			System.out.println("Element found at: "+foundIndex);	//O(1)
		
			input.close();
	 }
 }
