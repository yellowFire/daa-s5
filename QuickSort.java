/*

    AUTHOR: Suraj Naranatt; DATE: 7SEP2020

    Program implementing quick sort algorithm with middle element as partition.

*/

class QuickSort{

    void sort(int[] array, int lBound, int hBound){                         //O(n^2)

        if(lBound < hBound){                                                //O(1)                                           

            int partitionIndex = partition(array,lBound,hBound);            //O(n^2)

            sort(array,lBound,partitionIndex-1);                            //O((n/2)^2)    

            sort(array,partitionIndex+1,hBound);                            //O((n/2)^2)

        }

    }

    int partition(int[] array, int lBound, int hBound){

        int i = lBound, j = hBound - 1;                         //O(1)

        int partitionIndex = (lBound + hBound) / 2;             //O(2)

        int partition = array[partitionIndex];                  //O(2)

        swap(array,partitionIndex, hBound);                     //O(5)

        

        do{                                                     //O(n^2)

            while(array[i] < partition)                         //O(n) ∵ in the worst case, the partition turns out to be the largest key.

                i++;

            while(array[j] > partition)                         //O(n) ∵ in the worst case, the partition turns out to be the smallest key.

                j--;

            if(i < j)                                           //O(6)

                swap(array,i,j);

        }

        while(i<j);

        swap(array,i,hBound);                                   //O(5)

        return partitionIndex;                                  //O(1)

    }

    void swap(int[] array, int i, int j){   //O(5)

        int temp = array[i];    //0(2)

        array[i] = array[j];    //O(3)

        array[j] = temp;        //O(2)

    }

    public static void main(String[] args){

        int[] array = {42, 23, 74, 11, 65, 58, 94, 36, 99, 87};     //To show average-case complexity.

        int[] array2 = {99, 94, 87, 74, 65, 68, 42, 36, 23, 11};    //To show worst-case complexity.

        int[] array3 = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};      //To show best-case complexity.

        QuickSort qs = new QuickSort();

        

        System.out.println("Average Case Array");

        //Printing the original array.

        for(int i = 0; i < array.length; i++)

            System.out.print(""+array[i]+"|");

        System.out.println("\b ");

        System.out.println("Sorted 'array':");

        //Sorting 'array'.

        qs.sort(array,0,array.length-1);

        for(int i = 0; i < array.length; i++)

            System.out.print(""+array[i]+"|");

        System.out.println("\b ");

        System.out.println("Worst Case Array (descending order)");

        //Printing the original array.

        for(int i = 0; i < array2.length; i++)

            System.out.print(""+array2[i]+"|");

        System.out.println("\b ");

        System.out.println("Sorted 'array2':");

        //Sorting 'array2'.

        qs.sort(array2,0,array2.length-1);

        for(int i = 0; i < array2.length; i++)

            System.out.print(""+array2[i]+"|");

        System.out.println("\b ");

        System.out.println("Best Case Array (already sorted)");

        //Printing the original array.

        for(int i = 0; i < array3.length; i++)

            System.out.print(""+array3[i]+"|");

        System.out.println("\b ");

        System.out.println("Sorted 'array3':");

        //Sorting 'array3'.

        qs.sort(array3,0,array3.length-1);

        for(int i = 0; i < array3.length; i++)

            System.out.print(""+array3[i]+"|");

        System.out.println("\b ");

    }

}