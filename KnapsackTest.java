/*
	AUTHOR: Suraj Naranatt; DATE: 29/OCT/2020
	ABOUT: Program implementing solution to 0/1 and fractional knapsack problems.
	USAGE: java KnapsackTest <knapsackCapacity> <itemCount> <fractionalMode>
			'knapsackCapacity' and 'itemCount' are integers, while 'fractionalMode' is a boolean value [true/false].
*/

import java.util.Scanner;
import java.util.ArrayList;
import java.util.Collections;

class Item implements Comparable<Item> {
	float profit;
	int weight;
	float profitByWeight;

	Item() {
		profit = 0.0f;
		weight = 0;
		profitByWeight = 0.0f;
	}

	Item(int weight, float profit) {
		this.weight = weight;
		this.profit = profit;
		this.profitByWeight = profit / (float) weight;
	}

	float getProfitByWeightRatio() {
		return profitByWeight;
	}

	void setProfitByWeight(float profitByWeight) {
		this.profitByWeight = profitByWeight;
	}

	int getWeight() {
		return this.weight;
	}

	void setWeight(int weight) {
		this.weight = weight;
	}

	float getProfit() {
		return this.profit;
	}

	void setProfit(float profit) {
		this.profit = profit;
	}

	// Overriding toString() method to allow easy printing of attributes.
	@Override
	public String toString() {
		return "Weight:" + this.weight + "|Profit:" + this.profit + "|Ratio:" + this.profitByWeight;
	}

	/*
	 * This method is requried to allow the comparison of objects of Item class.
	 * Used in sorting. Comparison based on 'profitByWeightRatio'.
	 */
	@Override
	public int compareTo(Item anotherItem) {
		return Float.compare(this.getProfitByWeightRatio(), anotherItem.getProfitByWeightRatio());
	}
}

class Knapsack {
	/*
	 * 'MAX_ITEM_COUNT' is the maximum number of items available for selection.
	 * 'filledCapacity' measures the weight of items in the knapsack, while it is
	 * being filled. 'fractionalMode' is a boolean flag to determine which version
	 * of the knapsack problem is being solved, and is set to false by default.
	 * 'itemSpace' stores the Items before they are inserted and 'insertedItems'
	 * stores the Items after they have been inserted into the stack. 'MAX_CAPACITY'
	 * is a constant value, set at runtime, and is the maximum weight above which
	 * the knapsack cannot hold items.'filledProfit' is the profit obtained by
	 * filling the knapsack with items.'selectedItems' is a float array where the
	 * value at each position corresponds to how much of an item has been added to
	 * the knapsack, i.e. selectedItems[i] = fraction of item 'i' which was selected
	 * for insertion.
	 */
	int filledCapacity;
	boolean fractionalMode;
	ArrayList<Item> itemSpace, insertedItems;
	final int MAX_ITEM_COUNT, MAX_CAPACITY;
	float filledProfit;
	float[] selectedItems;

	Knapsack(int MAX_CAPACITY, int MAX_ITEM_COUNT, boolean fractionalMode) {
		this.MAX_CAPACITY = MAX_CAPACITY;
		this.MAX_ITEM_COUNT = MAX_ITEM_COUNT;
		this.fractionalMode = fractionalMode;
		itemSpace = new ArrayList<Item>(MAX_ITEM_COUNT);
		insertedItems = new ArrayList<Item>(MAX_ITEM_COUNT);
	}

	void addToKnapsack() {
		if (MAX_ITEM_COUNT == 0) {
			System.out.println("No items to insert. Try calling insertItems(int itemCount).");
		} else {
			filledCapacity = 0;
			filledProfit = 0.0f;
			int currentItemWeight;
			float currentItemProfit;

			// When the knapsack is of fractional type.
			if (fractionalMode) {
				float[] selectedItems = new float[itemSpace.size()];

				/*
				 * Inserting elements is complete. Now sort the elements by profit/weight ratio
				 * in descending order.
				 */
				Collections.sort(itemSpace, Collections.reverseOrder());
				System.out.println("Sorting Complete:");
				this.showKnapsack();

				for (int i = 0; i < itemSpace.size() && filledCapacity != MAX_CAPACITY; i++) {
					Item currentItem = itemSpace.get(i);
					currentItemWeight = currentItem.getWeight();
					currentItemProfit = currentItem.getProfit();
					if (filledCapacity + currentItemWeight <= MAX_CAPACITY) {
						/*
						 * This refers to the case when the knapsack can still accommodate the current
						 * item in its entirety. The item is also added to the 'insertedItems' arraylist
						 * and 'filledCapacity' and 'filledProfit' are also updated by adding the item's
						 * weight and profit respectively.
						 */
						/*
						 * 1.0 means that the item was inserted in its entirety. Item not broken into
						 * pieces.
						 */
						selectedItems[i] = 1.0f;
						filledProfit += currentItemProfit;
						filledCapacity += currentItemWeight;
						insertedItems.add(currentItem);
					} else {
						/*
						 * 'selectedItems[i]' is set to the value of fraction of the item to be added.
						 * And 'filledCapacity' is set to 'MAX_CAPACITY'. The profit obtained by the
						 * fractional item is added to the 'filledProfit'.
						 */
						selectedItems[i] = (float) (MAX_CAPACITY - filledCapacity) / (float) currentItemWeight;
						filledCapacity = MAX_CAPACITY;
						filledProfit += selectedItems[i] * currentItemProfit;
						Item partialItem = new Item();
						partialItem.setProfit(selectedItems[i] * currentItemProfit);
						partialItem.setWeight((int) selectedItems[i] * currentItemWeight);
						insertedItems.add(partialItem);
						break; // Because we don't need to add any more items. 'MAX_CAPACITY' attained.
					}
				}
				itemSpace = insertedItems;
			} else {
				// When the knapsack is of 0|1 type.
				// This approach only works with positive integer weights of items.

				/*
				 * This makes the method zeroOneKnapsack() dependent on it, since the last
				 * position definitely contains the element with the largest profit to weight
				 * ratio. Look at the zeroOneKnapsack() method, since it starts checking weights
				 * of higher indexed items first. This sort will allow items with higher
				 * profit-to-weight ratios to be in the higher indices.
				 */
				// Collections.sort(itemSpace);
				filledProfit = zeroOneKnapsack(MAX_ITEM_COUNT - 1, MAX_CAPACITY);
				// 'MAX_ITEM_COUNT-1' because arraylist index is from 0 to MAX_ITEM_COUNT-1.
				System.out.println("The maximum value which can be inserted = " + filledProfit);
			}

			// for(int i = 0; i < insertedItems.size(); i++){
			// System.out.println(""+insertedItems.get(i).toString()+"|");
			// }
		}
	}

	float zeroOneKnapsack(int maxItemCount, int maxCapacity) {
		Item currentItem = itemSpace.get(maxItemCount);

		if (maxItemCount == 0 || maxCapacity == 0) {
			return 0.0f;
		} else if (currentItem.getWeight() > maxCapacity) {
			return zeroOneKnapsack(maxItemCount - 1, maxCapacity);
		} else {
			return Math.max(
					currentItem.getProfit() + zeroOneKnapsack(maxItemCount - 1, maxCapacity - currentItem.getWeight()),
					zeroOneKnapsack(maxItemCount - 1, maxCapacity));
		}
	}

	void insertItems(int itemCount) {
		Scanner input = new Scanner(System.in);
		int inputWeight = 0;
		float inputProfit = 0;

		try {
			for (int i = 0; i < itemCount; i++) {
				System.out.print("Item:" + (i + 1) + ":Weight: ");
				inputWeight = input.nextInt();
				System.out.print("Item:" + (i + 1) + ":Profit: ");
				inputProfit = input.nextInt();

				/*
				 * If 'inputWeight' or 'inputProfit' is negative, change to positive.
				 * subtracting twice a negative number to a negative number, is same as adding
				 * the second number to the first negative number.
				 */
				itemSpace.add(new Item((inputWeight < 0) ? inputWeight -= 2 * inputWeight : inputWeight,
						(inputProfit < 0) ? inputProfit -= 2 * inputProfit : inputProfit));
				System.out.println("\n");
			}
		} catch (Exception e) {
			System.err.println("Please enter natural numbers for weights and profits.");
			input.close();
		}
	}

	void showKnapsack() {
		int MAX_ITEM_COUNT = itemSpace.size();
		System.out.println("ItemCount:" + MAX_ITEM_COUNT);
		for (int i = 0; i < MAX_ITEM_COUNT; i++) {
			System.out.println("Item:" + i + ": " + itemSpace.get(i).toString());
		}
		System.out.println("Max Profit: " + this.filledProfit);
		System.out.println("Max Weight: " + this.filledCapacity);
		System.out.println("");
	}
}

class KnapsackTest {
	/*
	 * Static method to print a line made of block characters. "\u2592" = ▒
	 */
	static void printBlockLine() {
		for (int i = 0; i < 20; i++, System.out.print("\u2592"))
			;
		System.out.println("");
	}

	public static void main(String[] args) {
		int itemCount = 0, knapsackCapacity = 0;
		String knapsackModeString;
		boolean knapsackMode = true; // The knapsack is fractional knapsack by default.

		try {
			knapsackCapacity = Integer.parseInt(args[0]);
		} catch (Exception e) {
			System.err.println("Try this: java KnapsackTest <knapsackCapacity> <itemCount> <mode>\n");
			System.err.println("\nNote: <knapsackCapacity> and <itemCount> must be positive integers.\n"
					+ "<mode> must be either true or false, where true implies the fractional knapsack and false for the 0|1 knapsack.");
			System.exit(1);
		}

		try {
			itemCount = Integer.parseInt(args[1]);
		} catch (NumberFormatException e) {
			System.err.println("<invalid itemCount selected>Please enter a natural number and retry.");
			System.exit(2);
		}

		try {
			knapsackModeString = args[2];
			/*
			 * If args[2] is not like "true" or "false", then throw an exception.
			 */
			if (!(knapsackModeString.compareToIgnoreCase("true") == 0
					|| knapsackModeString.compareToIgnoreCase("false") == 0)) {
				throw new Exception("Invalid Mode");
			} else {
				// The parseBoolean() function converts 'knapsackModeString' to the
				// corresponding boolean value.
				knapsackMode = Boolean.parseBoolean(knapsackModeString);
			}
		} catch (Exception e) {
			System.err.println("<invalid mode selected> Please enter mode as true or false.");
			System.exit(3);
		}

		// Printing the options selected for the knapsack.
		String fractionalOrZeroOne = knapsackMode == true ? "fractional" : "0/1";
		System.out.println("MaxItemCount = " + itemCount + " \u2591 " + "KnapsackCapacity = " + knapsackCapacity
				+ " \u2591 " + "Mode = " + fractionalOrZeroOne);
		Knapsack mySack = new Knapsack(knapsackCapacity, itemCount, knapsackMode);

		printBlockLine();

		mySack.insertItems(itemCount);
		mySack.addToKnapsack();

		printBlockLine();

		mySack.showKnapsack();
	}
}
