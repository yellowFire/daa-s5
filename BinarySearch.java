import java.util.Scanner;

class BinarySearch{
    int Search(int[] array, int key){
        int first = 0, last = array.length-1, mid;
        while(first <= last){
            mid = (first+last)/2;

            if(key == array[mid] )
                return mid;
            else if(key > array[mid]){
                first = mid+1;
            }
            else{
                last = mid-1;
            }
        }
        return -1;
    }
    public static void main(String[] args){
        int[] array = {1, 3, 7, 16, 24, 33, 40};
        Scanner input = new Scanner(System.in);
        int searchFor;

        System.out.print("Search for: ");
        searchFor = input.nextInt();

        System.out.println("Element found at: "+new BinarySearch().Search(array,searchFor));
        input.close();
    }
}